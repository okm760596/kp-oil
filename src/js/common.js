
const gnbBtn = document.querySelector(".btn_gnb");
const depth1BtnAll = document.querySelectorAll(".one-depth_item");
const linkDepthBtn = document.querySelector(".link-option_item");
const depth1ListAll = document.querySelectorAll(".one-depth_list > li");
const AllMenuBtn = document.querySelector(".btn_allmenu");
const allMenuDepthAll = document.querySelectorAll(".all-menu_item.childs");
let $scn1= 0, $scn2 = 0;

$(document).ready(function () {
    $(".open-modal").on("click", uiModal.open); //modal popup
    $(".open-modal-to").on("click", uiModal.openModalTo); //modal to modal popup
    $(document).mouseup(function (e) {
        uiModal.clickModalOut(e);
    });

    linkDepthBtn.addEventListener('click', uiGnb.linkOption);
    gnbBtn.addEventListener('click', uiGnb.menuOpen);
    AllMenuBtn.addEventListener('click', uiGnb.allMenuOpen);
    allMenuDepthAll.forEach(function(allMenuDepthBtn){
        allMenuDepthBtn.removeEventListener('click', uiGnb.allMenuDepthOpen);
        allMenuDepthBtn.addEventListener('click', uiGnb.allMenuDepthOpen);
    });
    $(window).on("resize",function(){
        resizeCheckPc();
        resizeCheckMo();
    });
    $(window).load(function() {
        resizeCheckPc();
        resizeCheckMo();
    });

    mainUiTab();
    
});


function resizeCheckPc(){
    var $body = $('body'), $winWidth = window.innerWidth; //$(window).width();
    if($winWidth >= 960) {
        
        $body.removeClass('menu_on');

        if($('#businessSwiper').length > 0) {
            businessSwiper.slideTo(0);
            //productsSwiper.slideTo(0);
        }
        
        if($scn1 < 1){
            depth1BtnAll.forEach(function(depth1Btn){
                depth1Btn.removeEventListener('click', uiGnb.depthOpen);
            });
            depth1ListAll.forEach(function(depthList){
                depthList.removeEventListener('mouseover', uiGnb.depthHover);
                depthList.addEventListener('mouseover', uiGnb.depthHover);
                depthList.removeEventListener('mouseout', uiGnb.depthLeave);
                depthList.addEventListener('mouseout', uiGnb.depthLeave);
            });
        }
        $scn1 += 1;
    }
}
function resizeCheckMo(){
    var $body = $('body'), $winWidth = window.innerWidth; //$(window).width();
    if($winWidth < 960) {
        if($scn2 < 1){
            depth1BtnAll.forEach(function(depth1Btn){
                depth1Btn.removeEventListener('click', uiGnb.depthOpen);
                depth1Btn.addEventListener('click', uiGnb.depthOpen);
            });
            depth1ListAll.forEach(function(depthList){
                depthList.removeEventListener('mouseover', uiGnb.depthHover);
                depthList.removeEventListener('mouseout', uiGnb.depthLeave);
            });
        }
        $scn2 += 1;
    }
}

function mainUiTab(){
    $('.btn_tab').click(function() {
        var tab = $(this).attr('data-tab');
        $("#"+tab).addClass('active').siblings().removeClass('active')
        $(this).parents('.bt-tab_wrap').find('.btn_tab').removeClass('active');
        $(this).addClass('active');
    });
}

var uiGnb = {
    menuOpen : function(){
        var $gnb = $('.gnb_wrap');
        if($('body').hasClass('menu_on') && $gnb.hasClass('active')){
            $('body').removeClass('menu_on');
            $gnb.addClass('hide');
            setTimeout(function(){
                $gnb.removeClass('active');
            }, 200);
        } else {
            $('body').addClass('menu_on');
            $gnb.removeClass('hide').addClass('active');
        }
        
    },
    depthOpen : function(){
        if($(this).siblings('.two-depth_list').length > 0) {
            if($(this).hasClass('active')){    
                $(this).removeClass('active').siblings('.two-depth_list').slideUp(100);
            } else {
                $('.two-depth_list').slideUp(100);
                $('.one-depth_item').removeClass('active');
                $(this).addClass('active').siblings('.two-depth_list').slideDown(300);
            }
        }
    },
    depthHover : function(){
        if($(this).find('.two-depth_list').length > 0) {
            $(this).find('.one-depth_item').addClass('active').siblings('.two-depth_list').addClass('on');
            $(this).parents('.header').addClass('on');
        }
    },
    depthLeave : function(){
        if($(this).find('.two-depth_list').length > 0) {
            $(this).find('.one-depth_item').removeClass('active').siblings('.two-depth_list').removeClass('on');
            $(this).parents('.header').removeClass('on');
        }
    },
    linkOption : function(){
        if($(this).siblings('.link-depth_list').length > 0) {
            if($(this).hasClass('active')){    
                $(this).removeClass('active').siblings('.link-depth_list').slideUp(100);
            } else {
                $('.link-depth_list').slideUp(100);
                $('.link-option_item').removeClass('active');
                $(this).addClass('active').siblings('.link-depth_list').slideDown(300);
            }
        }
    },
    allMenuOpen : function(){
        var allMenuWrap = $('#allMenuWrap');
        if($(this).hasClass('active')){    
            $(this).removeClass('active');
            allMenuWrap.removeClass('active');
            $('body').removeClass('all_menu_on');
        } else {
            $(this).addClass('active');
            allMenuWrap.addClass('active');
            $('body').addClass('all_menu_on');
        }
    },
    allMenuDepthOpen : function(){
        if($(this).siblings('.all-childs_wrap').length > 0) {
            if($(this).hasClass('active')){    
                $(this).removeClass('active').siblings('.all-childs_wrap').slideUp(100);
            } else {
                $(this).addClass('active').siblings('.all-childs_wrap').slideDown(300);
            }
        }
    },
}

var uiModal = {
    open : function(){
        var target = $(this);
        var modalId = target.data("modal");
        $("#" + modalId)
            .addClass("active")
            .attr("tabindex", 0)
            .focus();
        if ($("#" + modalId).find(".modal-title").length > 0) {
            $("#" + modalId).addClass("scrollH1");
        }
        $("body").addClass("scrollHide");
        $(".modal_close").on("click", {
            btn : target, 
            id : modalId
        }, uiModal.close);
      
        // if (target.data('anchor').length > 0) {
        //   alert(target.data('anchor'));
        // }
        uiModal.modalSetInit(modalId);
        return false;
    },
    openModalTo : function(){
        var modalId = $(this).data("modal");
        var target = $(this);

        $("#" + modalId)
            .addClass("active")
            //.css("background", "none")
            .attr("tabindex", 0);
        // .focus();

        $("#" + modalId).find(".modal_close").on("click", function () {
            $(this).parents(".modal").addClass('delay');
            setTimeout(function() {
                $(this).parents(".modal").removeClass("active").removeClass("delay").closest("body").addClass("scrollHide");
            }, 380);
            
            target.focus();
            $("body").removeClass("scrollHide");
        });

        uiModal.modalSetInit(modalId);
        // 221229 컨텐츠 앵커 처리
        if (target.data('anchor').length > 0) {
          document.getElementById('modalInner1').scrollIntoView();
        }
        return false;
    },
    close : function(e){
        var clickBtn = $(`[data-modal=${e.data.id}]`);
        var target = $(this);
        $(this).parents(".modal").addClass('delay');
        setTimeout(function() { 
            target.parents(".modal").removeClass("active").removeClass("scrollH1").removeClass("delay").attr("tabindex", 0);
        }, 380);
        
        e.data.btn.focus();
        if($(this).parents('.modal').hasClass('readyClose')){
            $(this).parents('.modal').removeClass('readyClose');
        } else {
            $("body").removeClass("scrollHide");
        }
        //removeDisable(clickBtn);
    },
    clickModalOut : function(e){
        var findClass = e.target.className;
        var $thisId = $('#'+e.target.id);
        var modal = $(".modal");

        if (findClass.match("modal") && findClass.match("active")) {
            $thisId.addClass('delay');
            setTimeout(function() { 
                $thisId.removeClass("active").removeClass('delay');
            }, 380);
            
            if($thisId.hasClass('readyClose')){
                $thisId.removeClass('readyClose');
            } else {
                $("body").removeClass("scrollHide");  
            }
        }
        
        if (e.target.id == "") return;
        var cilickBtn = $(`[data-modal=${e.target.id}]`);
        //console.log(cilickBtn);
    },
    modalSetInit : function(e){
        var $target = "#" + e;  
        // 팝업 scroll 초기화
        if($($target).find('.modal-con').length > 0) { 
            $($target).find('.modal-con').scrollTop(0);
        }
    }
    
}

var uiAccordion = {
    init : function(){
        uiAccordion.clickEvt();
    },
    closeAll : function(e){
        var $target = $('.accordion__list');
        $target.find('li').removeClass('active');
        $target.find('.expand').removeClass('active');
        $target.find('.accordion__body').slideUp(100);
        
    },
    thisOpen : function(e){
        e.addClass('active');
        e.parents('li').addClass('active');
        e.parents('li').find('.accordion__body').slideDown(100);
        //setTimeout(function() {
            //uiAccordion.autoScroll(e);
        //}, 110);
    },
    autoScroll : function(e){
        var $target = e.parents('li');
        var $navHeight = $('#header').outerHeight();
        $('html, body').animate({
            scrollTop : $target.offset().top - $navHeight
        }, 'fast');
    },
    clickEvt : function(){
        var $btn = $('.accordion__btn.expand');
        $btn.click(function(){
            if($(this).hasClass('active')){
                uiAccordion.closeAll($(this));
            } else {
                uiAccordion.closeAll($(this));
                uiAccordion.thisOpen($(this));
            }
        });
    }
}